clear, clc, close all



gen2 = loadrobot("kinovaJacoJ2N6S300","DataFormat","column");
%disp(gen2);

q1 = deg2rad(30);
q2 = deg2rad(60);
q3 = deg2rad(60);
q4 = deg2rad(60);
q5 = deg2rad(60);
q6 = deg2rad(60);
%A, alpha, d, theta
dh_table = [0 deg2rad(pi/2) 0.2755 q1;
    0.4100 deg2rad(pi) 0 q2;
    0 deg2rad(pi/2) -0.0098 q3;
    0 deg2rad(30) -0.2501 q4
    0 deg2rad(30) -0.0855 q5
    0 deg2rad(pi) -0.2027 q6];
%gui = interactiveRigidBodyTree(gen2, MarkerScaleFactor=0.5);
showdetails(gen2);
config = homeConfiguration(gen2);
disp(config)


%% Forward Kinematics

T = getTransform(gen2, config, 'j2n6s300_link_base', 'j2n6s300_end_effector')

% T_1 = getTransform(gen2, homeConfiguration(gen2),'j2n6s300_link_base');
% T_2 =  getTransform(gen2, homeConfiguration(gen2),'j2n6s300_link_2');
% T_F = T_2*T_1;
%show(gen2);